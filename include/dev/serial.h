#pragma once
#include <stddef.h>
#include <stdbool.h>
#ifdef _ARCH_x86_64_
#define COM1 0x3f8
#define COM2 0x2f8
#define COM3 0x3e8
#define COM4 0x2e8

typedef enum COM_PORT {
    COM_PORT1 = COM1,
    COM_PORT2 = COM2,
    COM_PORT3 = COM3,
    COM_PORT4 = COM4,
} com_port_t;
#else
typedef enum COM_PORT {
    COM_PORT1,
    COM_PORT2,
    COM_PORT3,
    COM_PORT4
} com_port_t;
#endif
bool serial_recieved(com_port_t port);
char read_serial(com_port_t port);

bool is_serial_transmit_empty(com_port_t port);
void write_serial(com_port_t port, char c);
void write_serial_string(com_port_t port, char* s);

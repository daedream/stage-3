#include <stddef.h>
#include <io/portio.h>

uint8_t inb(uint16_t port) {
    uint8_t ret;
    asm volatile ("inb %1, %0"
            : "=a" (ret)
            : "d" (port));
    return ret;
}

uint16_t inw(uint16_t port) {
    uint16_t ret;
    asm volatile ("inw %1, %0"
            : "=a" (ret)
            : "d" (port));
    return ret;
}

void outb(uint16_t port, uint8_t data) {
    asm volatile ("outb %1, %0"
            :
            : "d" (port), "a" (data));
}
void outw(uint16_t port, uint16_t data) {
    asm volatile ("outw %1, %0"
            :
            : "d" (port), "a" (data));
}

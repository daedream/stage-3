// Make sure that the current bootloader is bootboot before I start making the file
#ifdef BTLDR_BOOTBOOT
#include <ldr/bootboot.h>
#include <ldr/bootboot/bootboot.h>
#include <dev/serial.h>

void start_stage_3() {
    write_serial_string(COM_PORT1, "BOOTBOOT bootloader starting");
}



#endif

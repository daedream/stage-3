#include <dev/serial.h>
#include <arch.h>
#ifdef BTLDR_BOOTBOOT
#include <ldr/bootboot.h>
#else
#error "No bootloader defined!"
#endif

void _start() {
    // Output to serial
    write_serial_string(COM_PORT1, "Stage three started!\n");

    // Start the stage 3 implementation for the given bootloader
    start_stage_3();

    // Halt the CPU
    halt();
}
